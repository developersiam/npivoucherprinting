﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NPI_VoucherPrint.DAL;
using NPI_VoucherPrint.ViewModel;
using System.Data.Entity.Core.Objects;
using System.Data.Entity;

namespace NPI_VoucherPrint.Form
{
    /// <summary>
    /// Interaction logic for DocumentDetailWindow.xaml
    /// </summary>
    public partial class DocumentDetailWindow : Window
    {
        StecDBMSEntities stecDbms = new StecDBMSEntities();
        List<NPIDocDetailViewModel> _listNewDetail = new List<NPIDocDetailViewModel>();
        string _farmerCode;
        public DocumentDetailWindow(Guid documentID)
        {
            try
            {
                InitializeComponent();
                var listNPIDetail = stecDbms.NPI_DocumentDetail.Where(w => w.DocumentID == documentID).ToList();
                var listMatDocument = stecDbms.mats.Where(w => w.crop == DateTime.Today.Year && w.type == "FC" && !string.IsNullOrEmpty(w.farmer_code)).ToList();
                var listResult = from nDet in listNPIDetail
                                 join dMat in listMatDocument on nDet.BaleBarcode equals dMat.bc
                                 orderby dMat.baleno 
                                 select new NPIDocDetailViewModel
                                 {
                                     rcno = dMat.rcno,
                                     bc = dMat.bc,
                                     baleno = dMat.baleno,
                                     company = dMat.company,
                                     supplier = dMat.supplier,
                                     green = dMat.green,
                                     classify = dMat.classify,
                                     weightbuy = dMat.weightbuy,
                                     weight = dMat.weight,
                                     diff = dMat.weightbuy - dMat.weight
                                 };
                NPIDetailDataGrid.ItemsSource = listResult.OrderBy(o => o.baleno);
                TotalBaleTextBox.Text = listResult.Count().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public DocumentDetailWindow(string farmerCode, DateTime startDate, DateTime endDate)
        {
            try
            {
                InitializeComponent();
                _farmerCode = farmerCode;
                GenerateButton.Visibility = Visibility.Visible;
                var listNPIDetail = stecDbms.NPI_DocumentDetail.ToList();
                var listMatDocument = stecDbms.mats.Where(w => DbFunctions.TruncateTime(w.dtrecord) >= startDate &&
                                                               DbFunctions.TruncateTime(w.dtrecord) <= endDate &&
                                                               w.type == "FC" &&
                                                               w.farmer_code == farmerCode).ToList();
                var listResult = listMatDocument.Where(w => !listNPIDetail.Any(a => a.BaleBarcode == w.bc))
                                                .Select(dMat => new NPIDocDetailViewModel
                                                {
                                                    rcno = dMat.rcno,
                                                    bc = dMat.bc,
                                                    baleno = dMat.baleno,
                                                    company = dMat.company,
                                                    supplier = dMat.supplier,
                                                    green = dMat.green,
                                                    classify = dMat.classify,
                                                    weightbuy = dMat.weightbuy,
                                                    weight = dMat.weight,
                                                    diff = dMat.weightbuy - dMat.weight
                                                }).OrderBy(o => o.baleno).ToList();
                NPIDetailDataGrid.ItemsSource = listResult;
                _listNewDetail = listResult;
                TotalBaleTextBox.Text = listResult.Count().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

        }

        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var listNPI_Document = stecDbms.NPI_Document.Where(w => w.Crop == DateTime.Today.Year && w.FarmerCode == _farmerCode);
                int maxDocumentNumber = listNPI_Document.Count() < 1 ? 1 : listNPI_Document.Max(m => m.DocumentNumber) + 1;

                //Create new document
                Guid newGuid = Guid.NewGuid();
                stecDbms.NPI_Document.Add(new NPI_Document
                {
                    Crop = (Int16)DateTime.Today.Year,
                    FarmerCode = _farmerCode,
                    DocumentNumber = (Int16)maxDocumentNumber,
                    CreateDate = DateTime.Now,
                    CreateUser = Environment.UserName,
                    DocumentID = newGuid
                });

                //Create document detail
                foreach (var det in _listNewDetail)
                {
                    stecDbms.NPI_DocumentDetail.Add(new NPI_DocumentDetail
                    {
                        BaleBarcode = det.bc,
                        DocumentID = newGuid
                    });
                }
                stecDbms.SaveChanges();
                MessageBox.Show("Generate document success.");
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
