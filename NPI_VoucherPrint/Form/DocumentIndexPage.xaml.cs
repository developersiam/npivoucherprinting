﻿using NPI_VoucherPrint.DAL;
using NPI_VoucherPrint.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NPI_VoucherPrint.Form
{
    /// <summary>
    /// Interaction logic for DocumentIndexPage.xaml
    /// </summary>
    public partial class DocumentIndexPage : Page
    {
        DateTime createDate = DateTime.Now;
        StecDBMSEntities stecDbms = new StecDBMSEntities();
        public DocumentIndexPage()
        {
            InitializeComponent();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void DocumentDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            try
            {
                if (string.IsNullOrEmpty(DocumentDateDatePicker.Text)) return;
                NPIDataGrid.ItemsSource = null;
                DateTime recordDate = DocumentDateDatePicker.SelectedDate.Value.Date;

                var listNPIDetail = stecDbms.NPI_DocumentDetail.ToList();
                var listNPIDocument = stecDbms.NPI_Document.Where(w => EntityFunctions.TruncateTime(w.CreateDate) == recordDate).ToList();
                var listNPIFarmer = stecDbms.NPI_Farmer.ToList();
                var listMatDocument = stecDbms.mats.Where(w => w.crop.Value == DateTime.Today.Year &&
                                                               w.type == "FC" &&
                                                               !string.IsNullOrEmpty(w.farmer_code)).ToList();

                var listDocument = from nDet in listNPIDetail
                                   join nDoc in listNPIDocument on nDet.DocumentID equals nDoc.DocumentID
                                   join nFmr in listNPIFarmer on nDoc.FarmerCode equals nFmr.FarmerCode
                                   join dMat in listMatDocument on nDet.BaleBarcode equals dMat.bc
                                   select new NPIDocumentViewModel
                                   {
                                       Crop = nDoc.Crop,
                                       farmerCode = nDoc.FarmerCode,
                                       farmerName = nFmr.Prefix + " " + nFmr.FirstName + " " + nFmr.LastName,
                                       documentID = nDoc.DocumentID,
                                       documentNumber = nDoc.DocumentNumber,
                                       weightBuy = dMat.weightbuy.Value,
                                       weightReceive = dMat.weight.Value,
                                       createDate = nDoc.CreateDate
                                   };

                var listResult = listDocument.GroupBy(g => new { g.farmerCode, g.Crop, g.documentNumber })
                                             .Select(s => new NPIDocumentViewModel
                                             {
                                                 Crop = s.FirstOrDefault().Crop,
                                                 farmerCode = s.FirstOrDefault().farmerCode,
                                                 farmerName = s.FirstOrDefault().farmerName,
                                                 documentID = s.FirstOrDefault().documentID,
                                                 documentNumber = s.FirstOrDefault().documentNumber,
                                                 totalBale = s.Count(),
                                                 totalWeightBuy = s.Sum(sum => sum.weightBuy),
                                                 totalWeightReceive = s.Sum(sum => sum.weightReceive),
                                                 createDate = s.FirstOrDefault().createDate
                                             }).OrderBy(o => o.farmerCode).ThenBy(t => t.documentNumber).ToList();

                NPIDataGrid.ItemsSource = listResult;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GenerateDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            GenerateDocumentWindow window = new GenerateDocumentWindow();
            window.ShowDialog();
            ReloadDataGrid();
        }

        private void DocumentDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(DocumentDateDatePicker.Text)) return;
                Guid documentID = ((NPIDocumentViewModel)NPIDataGrid.SelectedItem).documentID.Value;
                DocumentDetailWindow window = new DocumentDetailWindow(documentID);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(DocumentDateDatePicker.Text)) return;
                if (NPIDataGrid.SelectedIndex <= -1) return;
                Guid documentID = ((NPIDocumentViewModel)NPIDataGrid.SelectedItem).documentID.Value;
                short crop = ((NPIDocumentViewModel)NPIDataGrid.SelectedItem).Crop;
                Voucher window = new Voucher(crop, documentID);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
