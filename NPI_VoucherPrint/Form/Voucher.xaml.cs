﻿using Microsoft.Reporting.WinForms;
using NPI_VoucherPrint.DAL;
using NPI_VoucherPrint.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace NPI_VoucherPrint.Form
{
    /// <summary>
    /// Interaction logic for Voucher.xaml
    /// </summary>
    public partial class Voucher : Window
    {
        StecDBMSEntities stecDbms = new StecDBMSEntities();
        public Voucher(short crop, Guid documentID)
        {
            InitializeComponent();

            var listGreen = stecDbms.sp_Receiving_SEL_Green(crop, "FC").ToList();
            var listNPIDetail = stecDbms.NPI_DocumentDetail.Where(w => w.DocumentID == documentID).ToList();
            var listMatDocument = stecDbms.mats.Where(w => w.crop == crop && w.type == "FC" && w.company == "1").ToList();
            var listResult = listMatDocument.Where(w => listNPIDetail.Any(a => a.BaleBarcode == w.bc))
                                            .Select(dMat => new NPIDocDetailViewModel
                                            {
                                                rcno = dMat.rcno,
                                                bc = dMat.bc,
                                                baleno = dMat.baleno,
                                                company = dMat.company,
                                                supplier = dMat.supplier,
                                                green = dMat.green,
                                                classify = dMat.classify,
                                                weightbuy = dMat.weightbuy,
                                                weight = dMat.weight,
                                                diff = dMat.weightbuy - dMat.weight
                                            }).OrderBy(o => o.baleno).ToList();

            List<NPIDocDetailViewModel> detailsList = new List<NPIDocDetailViewModel>();
            detailsList = (from dMat in listResult
                           join green in listGreen
                           on dMat.green equals green.green
                           select new NPIDocDetailViewModel
                           {
                               rcno = dMat.rcno,
                               bc = dMat.bc,
                               baleno = dMat.baleno,
                               company = dMat.company,
                               supplier = dMat.supplier,
                               green = dMat.green,
                               classify = dMat.classify,
                               weightbuy = dMat.weightbuy,
                               weight = dMat.weight,
                               diff = dMat.weightbuy - dMat.weight,
                               Price = green.p1.Value * dMat.weightbuy.Value,
                               p1 = green.p1
                           }).ToList();

            List<NPI_Document> listHeader = new List<NPI_Document>();
            listHeader = stecDbms.NPI_Document.Where(h => h.DocumentID == documentID).ToList();
            var documentHeader = from nDoc in listHeader
                    join mDoc in listMatDocument on nDoc.FarmerCode equals mDoc.farmer_code
                    select new NPIDocumentViewModel
                    {
                        Crop = nDoc.Crop,
                        farmerCode = nDoc.FarmerCode,
                        documentNumber = nDoc.DocumentNumber,
                        recordDate = nDoc.CreateDate,
                        supplier = mDoc.supplier
                    };
            
            ReportViewer.Reset();

            ReportDataSource ReportViewerDataSourceHeader = new ReportDataSource();
            ReportDataSource ReportViewerDataSourceDetail = new ReportDataSource();

            ReportViewerDataSourceHeader.Value = documentHeader.Take(1);
            ReportViewerDataSourceHeader.Name = "NPIDocumentHeader";

            ReportViewerDataSourceDetail.Value = detailsList;
            ReportViewerDataSourceDetail.Name = "NPIDocDetailViewModel";

            ReportViewer.LocalReport.DataSources.Add(ReportViewerDataSourceHeader);
            ReportViewer.LocalReport.DataSources.Add(ReportViewerDataSourceDetail);

            ReportViewer.LocalReport.ReportEmbeddedResource = "NPI_VoucherPrint.Report.Voucher.rdlc";
            ReportViewer.RefreshReport();
        }
    }
}
