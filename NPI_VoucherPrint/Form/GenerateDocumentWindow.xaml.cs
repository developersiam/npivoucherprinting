﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NPI_VoucherPrint.DAL;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using NPI_VoucherPrint.ViewModel;

namespace NPI_VoucherPrint.Form
{
    /// <summary>
    /// Interaction logic for GenerateDocumentWindow.xaml
    /// </summary>
    public partial class GenerateDocumentWindow : Window
    {
        StecDBMSEntities stecDbms = new StecDBMSEntities();
        List<NPIDocumentViewModel> _listNewDocument = new List<NPIDocumentViewModel>();
        List<mat> _listNewDetail = new List<mat>();
        public GenerateDocumentWindow()
        {
            InitializeComponent();
        }

        //private void RecordDateDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    ReloadDataGrid();
        //}

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadDataGrid();
        }

        private void ReloadDataGrid()
        {
            try
            {
                if (string.IsNullOrEmpty(RecordDateDatePicker.Text) || string.IsNullOrEmpty(RecordDate2DatePicker.Text)) return;
                DateTime startDate = RecordDateDatePicker.SelectedDate.Value.Date;
                DateTime endDate = RecordDate2DatePicker.SelectedDate.Value.Date;
                NPIDocumentDataGrid.ItemsSource = null;
                if (endDate < startDate) return;

                var listGenerated = stecDbms.NPI_DocumentDetail.ToList();
                var listDocument = stecDbms.mats.Where(w => w.type == "FC" && w.farmer_code != null && 
                                                            DbFunctions.TruncateTime(w.dtrecord.Value) >= startDate &&
                                                            DbFunctions.TruncateTime(w.dtrecord.Value) <= endDate).ToList();
                var listUnGenerate = listDocument.Where(w => !listGenerated.Any(a => a.BaleBarcode == w.bc)).ToList();
                var listFiltered = listUnGenerate.GroupBy(g => g.farmer_code).Select(s => new NPIDocumentViewModel
                {
                    Crop = Convert.ToInt16(s.FirstOrDefault().crop),
                    farmerCode = s.FirstOrDefault().farmer_code,
                    totalBale = s.Count(),
                    totalWeightBuy = s.Sum(sum => sum.weightbuy),
                    totalWeightReceive = s.Sum(sum => sum.weight),
                    recordDate = s.FirstOrDefault().dtrecord
                }).ToList();
                var listResult = from doc in listFiltered
                                     join farmer in stecDbms.NPI_Farmer.ToList()
                                     on doc.farmerCode equals farmer.FarmerCode
                                     select new NPIDocumentViewModel
                                     {
                                         Crop = doc.Crop,
                                         farmerCode = doc.farmerCode,
                                         farmerName = farmer.Prefix + " " + farmer.FirstName + " " + farmer.LastName,
                                         totalBale = doc.totalBale,
                                         totalWeightBuy = doc.totalWeightBuy,
                                         totalWeightReceive = doc.totalWeightReceive,
                                         recordDate = doc.recordDate
                                     };
                //new Document
                _listNewDocument = listFiltered;
                //new Document Detail
                _listNewDetail = listUnGenerate;
                GenerateButton.Content = string.Format("Generate ({0})", _listNewDocument.Count());
                NPIDocumentDataGrid.ItemsSource = listResult;
                TotalDocTextBox.Text = listResult.Count().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DocumentDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(RecordDateDatePicker.Text) || string.IsNullOrEmpty(RecordDate2DatePicker.Text)) return;
                DateTime startDate = RecordDateDatePicker.SelectedDate.Value.Date;
                DateTime endDate = RecordDate2DatePicker.SelectedDate.Value.Date;
                string farmerCode = ((NPIDocumentViewModel)NPIDocumentDataGrid.SelectedItem).farmerCode;
                DocumentDetailWindow window = new DocumentDetailWindow(farmerCode, startDate, endDate);
                window.ShowDialog();
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!_listNewDocument.Any()) return;
                foreach (var doc in _listNewDocument)
                {
                    //Create new document
                    int maxDocumentNumber;
                    var listNPI_Document = stecDbms.NPI_Document.Where(w => w.Crop == doc.Crop && w.FarmerCode == doc.farmerCode);
                    maxDocumentNumber = listNPI_Document.Count() < 1 ? 1 : listNPI_Document.Max(m => m.DocumentNumber) + 1;

                    Guid newGuid = Guid.NewGuid();
                    stecDbms.NPI_Document.Add(new NPI_Document
                    {
                        Crop = doc.Crop,
                        FarmerCode = doc.farmerCode,
                        DocumentNumber = (Int16)maxDocumentNumber,
                        CreateDate = DateTime.Now,
                        CreateUser = Environment.UserName,
                        DocumentID = newGuid
                    });
                    var _listDocumentDetail = _listNewDetail.Where(w => w.farmer_code == doc.farmerCode).ToList();
                    foreach (var det in _listDocumentDetail)
                    {
                        stecDbms.NPI_DocumentDetail.Add(new NPI_DocumentDetail
                        {
                            BaleBarcode = det.bc,
                            DocumentID = newGuid
                        });
                    }
                    stecDbms.SaveChanges();
                }
                MessageBox.Show("Generate document success.");
                ReloadDataGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
