﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPI_VoucherPrint.DAL;

namespace NPI_VoucherPrint.ViewModel
{
    public class NPIDocDetailViewModel : mat
    {
        public decimal? diff { get; set; }
        public Nullable<decimal> p1 { get; set; }
        public decimal Price { get; set; }
    }
}
