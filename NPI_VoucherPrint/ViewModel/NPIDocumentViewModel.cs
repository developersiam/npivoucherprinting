﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPI_VoucherPrint.ViewModel
{
    public class NPIDocumentViewModel
    {
        public short Crop { get; set; }
        public string farmerCode { get; set; }
        public string farmerName { get; set; }
        public decimal weightReceive { get; set; }
        public decimal weightBuy { get; set; }
        public int? totalBale { get; set; }
        public decimal? totalWeightBuy { get; set; }
        public decimal? totalWeightReceive { get; set; }
        public DateTime? recordDate { get; set; }
        public string baleBarcode { get; set; }
        public short documentNumber { get; set; }
        public Guid? documentID { get; set; }
        public DateTime createDate { get; set; }
        public string supplier { get; set; }
    }
}
